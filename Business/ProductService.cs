using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using McpProject.Data;
using McpProject.Model;
using System.Linq;
using MySql.Data.MySqlClient;

namespace McpProject.Business
{
    public class ProductService
    {
        public const string GET_PRODUCTS = "getProducts";
        CalculatorContext context { get; set; }

        public ProductService()
        {
            string connStr = "server=lgg2gx1ha7yp2w0k.cbetxkdyhwsb.us-east-1.rds.amazonaws.com;userid=ka4ar7n5ojekwikk;pwd=p4cidi4cx7drueux;port=3306;database=x0b0ufekwm5bcx0h;sslmode=none;";
            context = new CalculatorContext(connStr);
        }

        public IEnumerable<ProductCategory> GetProducts()
        {
            List<ProductCategory> products = new List<ProductCategory>();
            List<Dictionary<string, object>> rows = context.getDataByProcedure("spGetCategories", null);

            if(rows != null && rows.Any())
            {
                (from Dictionary<string, object> row in rows select row.ToModel<ProductCategory>()).ToList().AsParallel().ForAll(item =>
                {
                    item.ProductsList = (List<Product>) GetResources(item.Id);
                    products.Add(item);
                });
            }

            return products;
        }

        public IEnumerable<Product> GetResources(int categoryId)
        {
            List<Product> resources = new List<Product>();
            List<MySqlParameter> parameters = null;
            List<Dictionary<string, object>> rows = context.getDataByProcedure("spGetProducts", parameters);

            if(rows != null && rows.Any())
            {
                (from Dictionary<string, object> row in rows select row.ToModel<Product>()).Where(item => item.CategoryId == categoryId).ToList().AsParallel().ForAll(item =>
                {
                    resources.Add(item);
                });
            }

            return resources;
        }

        public IEnumerable<PropertyBase> GetProperties(int id)
        {
            List<PropertyBase> properties = new List<PropertyBase>();
            //List<MySqlParameter> parameters = new List<MySqlParameter>();
            //parameters.Add(new MySqlParameter("@property_id", id));
            List<MySqlParameter> parameters = null;
            List<Dictionary<string, object>> rows = context.getDataByProcedure("spGetProductProperties", parameters);
            return properties;
        }
    }
}
