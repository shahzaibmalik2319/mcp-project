import { Component, Input, Output, OnInit } from '@angular/core';
import { LoaderService } from '../Services/loader.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {


    showContent = this.loaderService.getLoader();

    constructor(public loaderService: LoaderService, private slimLoadingBarService: SlimLoadingBarService) {
        this.slimLoadingBarService.start(() => {
            console.log("Start")
        });
    }

    ngOnit() {
    }
}
