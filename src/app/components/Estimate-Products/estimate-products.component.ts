import { Component, Input, AfterViewInit, ViewChild, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IMyDpOptions, IMyOptions } from 'mydatepicker';


// importing service
import { AppService } from '../Services/app.service';
import { ApiService } from '../Services/api.service';
import { NotificationService } from '../Services/notification.service'
import { AddComponentServive } from "../Services/add-component.service";

@Component({
  selector: 'estimate-products',
  templateUrl: './estimate-products.component.html',
  styleUrls: ['./estimate-products.component.css']
})
export class EstimateProductsComponent {
  date = new Date();
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    disableUntil: { year:this.date.getFullYear() ,month:this.date.getMonth() + 1,day:this.date.getDate() - 1 }, 
  };


  @Input()
  products: any = [];
  showDiv = true;
  priceDistribution: any = 744;
  calRate: any;


  startDate:any;
  endDate:any;
  grandTotal: any = 0;

  constructor(private activateRoute: ActivatedRoute, public service: AppService,
     public apiService: ApiService, public NotificationService: NotificationService) {
  }



  ngOnChanges(changes: any) {
    console.log("Change", changes)
    // when changes to estimation resources then make the grandtotal
    this.makeGrandTotal();    
  }

  // Collapse all the resources
  collapseAll() {
    for (let i = 0; i < this.products.length; i++) {
      if (this.products[i].isCollapse) {
        this.products[i].isCollapse = false;
      }
    }
  }

  // Open all the resources
  openAll() {
    for (let i = 0; i < this.products.length; i++) {
      if (!this.products[i].isCollapse) {
        this.products[i].isCollapse = true;
      }
    }
  }

  // Delete all the resources
  deleteAll() {
    this.products = []
    localStorage.removeItem('products');
    this.service.deleteProducts();
    history.pushState(null, "", location.href.split("?")[0]);
    this.makeGrandTotal();
  }

  // Clone a resource
  cloneResource(resource: any) {
    let item = resource;
    console.log(item);
    item.isCollapse = false;
    this.products.push(Object.assign({}, item));
    this.service.setProducts(item);
  }

  // delete on the basis of resource index
  deleteResource(resourceObject: any) {
    console.log("Resource", resourceObject)
    this.products.splice(resourceObject.index, 1);
    this.service.deleteProducts(resourceObject.index);
    this.makeGrandTotal();

  }



  // Make grandtotal of all the resources
  makeGrandTotal() {
    this.grandTotal = 0;
    if (this.products && this.products.length > 0) {
      for (let i = 0; i < this.products.length; i++) {
        this.grandTotal += Number(this.products[i].totalCost)
      }
      this.grandTotal = this.grandTotal.toFixed(2);
      console.log(this.grandTotal)
    }
  }

  // change start date of period of performance
  onStartDateChanged(date:any){
      this.startDate = date.date
      console.log("Start Date",this.startDate);

    }

    // change end date of peirod of performance
    onEndDateChanged(date:any){
      this.endDate = date.date
      console.log("End Date",this.endDate);
      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date(this.startDate.date.year,this.startDate.date.month,this.startDate.date.day);
      var secondDate = new Date(this.endDate.year,this.endDate.month,this.endDate.day);
      var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
      console.log("Total Days",diffDays)
      
    }
  
  ngOnInit() {
    this.makeGrandTotal();
  }



}