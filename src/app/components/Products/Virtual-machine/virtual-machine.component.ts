import { Component, Input,Output,EventEmitter } from '@angular/core';
import { AppService } from '../../Services/app.service';
import { ApiService } from "../../Services/api.service";


// import interface
import { ComponentInterface } from "../../component-interface/component-interface.component";

@Component({
    selector: 'virtual-machine',
    templateUrl: './virtual-machine.component.html',
    styleUrls: ['./virtual-machine.component.css']
})
export class VirtualMachineComponent implements ComponentInterface {

    @Input()
    resource: any;
    @Output()
    changeCost = new EventEmitter();
    @Output()
    onClone = new EventEmitter();
    @Output()
    onDelete = new EventEmitter();
    @Input()
    index: any

    storageTypes = new Array;
    machineConfig = {
        OS:'',
        cpu:0,
        ram:0
    };

    OStypes:any = new Array;
    
    // priceDistribution: any = 744;   // default '744'
    calRate: any;
    maxValues = {
        hour: 744,
        day: 31,
        month: 1
    };
    myModel = { email: " john.doe@example.com", password: " just testing ", rememberMe: false };
    // select form sehema
    sehema = {
        "properties": {
            "Service Type": {
                "type": "string",
                "description": "Service Type",
                "widget": "select",
                "oneOf": [{
                    "description": "Plan A", "enum": ["type 0"]
                }, {
                    "description": "Plan B", "enum": ["type 1"]
                }],
                "default": "type 0"
            },
        },
    }
    pricingMethod = 'hour';         // default 'hour'
    max = 744;                      // max value for input
    constructor(public appservice: AppService, public apiService: ApiService) {
        // this.resource = this.resource
    }



    // Change Prize Distribution type for hour, day and month
    changePriceDistribution(pricingMethod: any) {
        console.log(this.resource.priceDistribution.name)
        switch (this.resource.priceDistribution.name) {
            case 'hour':
                this.calRate = this.resource.pricePerHour;
                this.resource.priceDistribution.distibution = this.maxValues.hour
                this.max = this.maxValues.hour
                break;
            case 'day':
                this.calRate = this.resource.pricePerDay
                this.resource.priceDistribution.distibution = this.maxValues.day
                this.max = this.maxValues.day
                break;
            case 'month':
                this.calRate = this.resource.pricePerMonth
                this.resource.priceDistribution.distibution = this.maxValues.month
                this.max = this.maxValues.month
                break;
        }
        this.updateTotalCost();
    }

    // Updating the total cost of the resource
    updateTotalCost() {
        this.resource.totalCost = this.resource.quantity * this.resource.priceDistribution.distibution * this.calRate
        this.resource.totalCost = this.resource.totalCost.toFixed(2);
        console.log(this.resource)
        console.log(this.resource.priceDistribution.distibution)
        this.appservice.updateProducts(this.resource, this.index)
        this.changeCost.emit()
    }

    // Clone the current resource
    cloneResource() {
        this.onClone.emit(this.resource);
    }

    // Delete the current resource
    deleteResource() {
        this.onDelete.emit({ index: this.index, resource: this.resource })
    }
    // set calc rate
    setCalRate() {
        if (this.resource.priceDistribution.name === 'hour') {
            this.calRate = this.resource.pricePerHour;
        }
        else if (this.resource.priceDistribution.name === 'day') {
            this.calRate = this.resource.pricePerDay
        }
        else if (this.resource.priceDistribution.name === 'month') {
            this.calRate = this.resource.pricePerMonth
        }
    }
    ngOnInit() {
        this.getOSType();
        this.getStorageType();
        console.log(this.resource)
        // this.setCalRate();
        // this.updateTotalCost();
    }
    addStorage(){
        let length = this.storageTypes.length;
        console.log(length)        
        length += 1
        let newStorage = { name: 'Tier ' + length + ' Block Storage'}
        this.storageTypes.push(newStorage);
    }
    getOSType(){
        this.apiService.get('api/lookup/lkup_os_type').subscribe((data:any)=>{
          let value = JSON.parse(data.value);
          this.OStypes = value;
          console.log("OS Type",this.OStypes);  
          this.machineConfig.OS = this.OStypes[0].name;
          this.machineConfig.cpu = 1;    
          this.machineConfig.ram = 1;    
        },(error:any)=>{
          console.log(error)
        })
      }
      getStorageType(){
          this.apiService.get('api/lookup/lkup_storage_type').subscribe((data)=>{
              let types = JSON.parse(data.value);
              this.storageTypes = types;
              console.log("Types",this.storageTypes);
          },(error)=>{
              console.log(error)
          })
      }



}
