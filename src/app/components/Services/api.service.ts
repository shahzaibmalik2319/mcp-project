import { Injectable, Inject } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';
// import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  //  http://milcloudpc.azurewebsites.us/";
  // public baseUrlAPI:string;
  public baseUrlAPI: string = 'http://localhost:5000/';


  constructor(private _http: Http) {
    // this.baseUrlAPI = environment.apiUrl;
    // console.log(environment.apiUrl);
    //this.baseUrlAPI = "http://"+ window.location.host + "/";
    console.log("---------------");
  }

  get(endPoint: any) {
    return this._http.get(this.baseUrlAPI + endPoint)
      .map(data => {
        data.json();
        console.log("I CAN SEE DATA GET HERE: ", data.json());
        return data.json();
      });
  }

  post(endPoint: any, data: any) {
    console.log(data);
    return this._http.post(this.baseUrlAPI + endPoint, data)
      .map(data => {
        data.json();
        console.log("I CAN SEE DATA POST HERE: ", data.json());
        return data.json();
      });
  }

  delete(endPoint: any) {
    return this._http.delete(this.baseUrlAPI + endPoint)
      .map(data => {
        data.json();
        console.log("I CAN SEE DATA DELETE HERE: ", data.json());
        return data.json();
      });
  }

  update(endPoint: any, data: any) {
    return this._http.put(this.baseUrlAPI + endPoint, data)
      .map(data => {
        data.json();
        console.log("I CAN SEE DATA UPDATE HERE: ", data.json());
        return data.json();
      });
  }
  ngOnInit() {

  }


}
