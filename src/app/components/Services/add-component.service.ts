import { Injectable } from '@angular/core';

import { TypeComponent } from './type-component';

// importing components
import { VirtualMachineComponent } from "../Products/Virtual-machine/virtual-machine.component";
import { AdditionalCPUComponent } from "../Products/Additional-CPU/addtional-cpu.component";
import { AdditionalRAMComponent } from "../Products/Additional-RAM/addtional-ram.component";
import { StorageComponent } from "../Products/Storage/storage.component";
import { RoutableExternalIPComponent } from "../Products/Routable-External-IP/routable-external-ip.component";
import { VPNServiceComponent } from "../Products/VPN-Service/vpn-service.component";
import { NetworkLogsComponent } from "../Products/Network-logs/network-logs.component";
import { ServerLogsComponent } from "../Products/Server-logs/server-logs.component";
import { VulnerabilityComponent } from "../Products/Vulnerability/vulnerability.component";
import { ObjectWebStorageComponent } from "../Products/Object-web-storage/object-web-storage.component";
import { StandardDailyBackupComponent } from "../Products/Standard-daily-backup/standard-daily-backup.component";
import { OnDemandBackupComponent } from "../Products/On-demand-backup/on-demand-backup.component";
import { PatchManagemantComponent } from "../Products/Patch-managemant/patch-managemaent.component";
import { WindowsComponent } from "../Products/Windows/windows.component";
import { RedHatEnterpriseLinuxComponent } from "../Products/Red-hat-enterprise-linux/red-hat-enterprise-linux.component";
import { CentosComponent } from "../Products/Centos/centos.component";

@Injectable()
export class AddComponentServive {
    constructor(){}


    getComponents(){
        return [
            new TypeComponent(AdditionalCPUComponent,{},'cpu'),
            new TypeComponent(AdditionalRAMComponent,{},'ram'),
            new TypeComponent(StorageComponent,{},'storage'),
            new TypeComponent(RoutableExternalIPComponent,{},'reip'),
            new TypeComponent(VPNServiceComponent,{},'vpn'),
            new TypeComponent(NetworkLogsComponent,{},'nlogs'),
            new TypeComponent(ServerLogsComponent,{},'slogs'),
            new TypeComponent(VulnerabilityComponent,{},'vul'),
            new TypeComponent(ObjectWebStorageComponent,{},'owstorage'),
            new TypeComponent(StandardDailyBackupComponent,{},'sdbackup'),
            new TypeComponent(OnDemandBackupComponent,{},'odbackup'),
            new TypeComponent(PatchManagemantComponent,{},'patchmanagement'),
            new TypeComponent(WindowsComponent,{},'win'),
            new TypeComponent(RedHatEnterpriseLinuxComponent,{},'linux'),
            new TypeComponent(CentosComponent,{},'centos'),

        ]
    }
}