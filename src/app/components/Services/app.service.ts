import { Injectable, OnInit } from "@angular/core";

@Injectable()

export class AppService implements OnInit {
    public products: any = [];
    public inputSearchValue: any;
    public currentProduct: any
    constructor() {
    }

    // set products in service variable and in localStorage
    setProducts(item: any) {
        this.products.push(item);
        localStorage.setItem('products', JSON.stringify(this.products));
    }

    updateProducts(product: any, index: any) {
        this.products.splice(index, 1, product)
        console.log('products', this.products)
        localStorage.setItem('products', JSON.stringify(this.products));
    }

    // getting all products form localStorage if available
    getProducts() {
        if (localStorage.getItem('products')) {
            let items: any = localStorage.getItem('products');
            return JSON.parse(items);
        }
    }

    // delete all products if index is not available if available this delete only that product
    deleteProducts(index?: any) {
        // debugger;
        if (index !== undefined) {
            this.products.splice(index, 1)
            console.log(this.products);
            localStorage.setItem('products', JSON.stringify(this.products));
        }
        else if (index === undefined) {
            this.products = [];
        }
    }


    // set the search value in the service variable
    setSearchValue(value: any) {
        this.inputSearchValue = value;
        return this.getSearchValue();
    }

    // getting search value from the service varaible
    getSearchValue() {
        console.log(this.inputSearchValue);
        return this.inputSearchValue;
    }
    // setting current product
    setCurrentProduct(product: any) {
        this.currentProduct = product;
    }
    // getting current product
    getCurrentProduct() {
        return this.currentProduct;
    }
    // get previous product
    getPreviousProducts() {
        var product: any = localStorage.getItem('products');
        return JSON.parse(product)
    }
    ngOnInit() {

    }
}