import { Injectable, OnInit } from "@angular/core";

@Injectable()

export class LoaderService {
    loader = { status: false }

    constructor(){}

    getLoader(){
        return this.loader
    }
    showLoader(){
        this.loader.status = true;
    }
    hideLoader(){
        this.loader.status = false;
    }
}