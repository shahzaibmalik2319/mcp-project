import { Injectable } from "@angular/core";

@Injectable()

export class NotificationService {
    // default notification data set
    notificationData = { message: 'message', picUrl: './../../../assets/img/virtual-icon.png' ,status: false,linkUrl:'' }

    // notification time 
    notificationTime = 3000;
    constructor() {
    }
    // setting the notification data to the service
    setNotification(data:any){
        this.notificationData.message = data.message;
        this.notificationData.linkUrl = data.linkUrl
        this.showNotification();
        // hide notification after time that decalre in the notificationTime
        setTimeout(()=>{
            this.hideNotification()
        },this.notificationTime)
    }

    // get notification data
    getNotification(){
        return this.notificationData;
    }

    // show notification on behave of its status
    showNotification(){
        this.notificationData.status = true;
    }

    // hide notification on behave of its status
    hideNotification(){
        this.notificationData.status = false;
    }

}