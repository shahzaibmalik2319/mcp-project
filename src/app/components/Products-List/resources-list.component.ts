import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'resources-list',
  templateUrl: './resources-list.component.html',
  styleUrls: ['./resources-list.component.css']
})

export class ResourcesListComponent {
  @Input()
  products: any;
  @Output()
  selectedProcduct = new EventEmitter();
  
  constructor(private activateRoute: ActivatedRoute, private location: Location) {
  }

  // emit the selected resource 
  addProduct(product: any) {
    this.selectedProcduct.emit(product);
  }
}