import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {
    @Output('change') inputChange = new EventEmitter();
    @Output() navBar = new EventEmitter();
    @Input()
    showNav: any
    searchFrom: FormGroup;
    constructor(fb: FormBuilder) {
        this.searchFrom = fb.group({
            'searchValue': [null, Validators.required]
        })
    }
    // show nav menu back to the list
    back() {
        this.searchFrom.reset();
        this.showNav = false;
        this.navBar.emit(false)
    }
}
