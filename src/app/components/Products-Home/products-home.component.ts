import { Component, Input, AfterViewInit, ViewChild, ComponentFactoryResolver, OnDestroy } from '@angular/core';

// importing service
import { AppService } from '../Services/app.service';
import { ApiService } from '../Services/api.service';
import { NotificationService } from '../Services/notification.service'
import { AddComponentServive } from "../Services/add-component.service";
import { LoaderService } from "../Services/loader.service";
@Component({
    selector: 'products-home',
    templateUrl: './products-home.component.html',
    styleUrls: ['./products-home.component.css']

})
export class ProductsHomeComponent implements AfterViewInit, OnDestroy {
    searchInput: any;
    productsAvailable: any;
    showNavBar: boolean = true;
    productSelected: any;
    showContent:any;
    constructor(public appservice: AppService, public apiService: ApiService, public NotificationService: NotificationService,
        public loaderService: LoaderService) {
        // console.log = function(){}
        this.showContent = this.loaderService.getLoader()
    }
    ngOnInit() {
        if (localStorage.getItem('products')) {
            this.appservice.products = this.appservice.getPreviousProducts();
        }
        this.getSelectedProducts();
    }
    // products got from navmenu
    selectedItem(data: any) {
        this.productsAvailable = data;
    }
    getSelectedProducts() {
        this.productSelected = this.appservice.getProducts();
    }
    // add resource to cart
    addProductToList(product?: any) {
        // productSelected to pass to estimate component
        this.appservice.setProducts(product);
        this.productSelected = this.appservice.getProducts();
        this.NotificationService.setNotification({ message: product.title + ' ' + 'added', linkUrl: '#' + product.shortKey });
    }

    // Update the search value and search for that product
    update($event: any) {
        this.searchInput = $event.target.value
        console.log("Search Input", this.searchInput);
        if (this.searchInput == '') {
            this.showNavBar = true;
        }
        else {
            this.showNavBar = false
        }
    }

    //  show navbar and and get the current product from the service
    show() {
        this.showNavBar = true;
        var products: any = this.appservice.getCurrentProduct();
        console.log(products);
        this.productsAvailable = products.productsList;
    }
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }

}