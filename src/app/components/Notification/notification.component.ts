import { Component, Input,Output,OnChanges, OnInit, SimpleChanges,SimpleChange } from '@angular/core';
import { NotificationService } from '../Services/notification.service'


@Component({
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})
export class NotificationComponent {
    
notification:any = {};

constructor(public NotificationService: NotificationService){}    

ngOnInit(){
    // get notification data from the notification service
    this.notification = this.NotificationService.getNotification();
}


}
