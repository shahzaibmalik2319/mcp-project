import { OnInit, Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppService } from "../Services/app.service";
import { ApiService } from "../Services/api.service";
import { LoaderService } from "../Services/loader.service";
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})



export class NavMenuComponent {


    searchValue: any;
    searchedResources: any = []
    @Output()
    selectedItem = new EventEmitter();
    @Input()
    searchKey: any;

    selectedProduct: any;
    products: any = [];

    constructor(private activatedRoute: ActivatedRoute, private router: Router, public appService: AppService, public apiService: ApiService,
        private loaderService: LoaderService, private slimLoadingBarService: SlimLoadingBarService) {
    }

    ngOnChanges(changes: any) {
        // when serach input changes this funtion call and search for that resource and if the search input is 
        // empty so its takes prevoius product from the service and set to the resource list and show the navbar

        console.log("component updated: ", changes.searchKey.currentValue);
        this.getSearchProducts(changes.searchKey.currentValue)
        if (changes.searchKey.currentValue == '') {
            console.log("Empty", changes.searchKey.currentValue)
            var product = this.appService.getCurrentProduct();
            this.selectedProduct = product;
            console.log("product", this.selectedProduct)
            this.selectedItem.emit(this.selectedProduct.productsList);
        }
    }
    // get products from API and set the current product to the service
    getAllProducts() {
        this.apiService.get('api/product').subscribe((data) => {
            this.products = data;
        }, (error) => {
            console.log(error)
        }, () => {
            this.selectedProduct = this.products[0];
            this.appService.setCurrentProduct(this.selectedProduct);
            this.selectedItem.emit(this.products[0].productsList);
            this.loaderService.loader.status = true;
            this.slimLoadingBarService.complete();
        });
    }
    // get searched resources
    getSearchProducts(value: string) {
        if (value !== undefined && value !== '') {
            if (this.searchedResources.length > 0) {
                this.searchedResources = []
            }
            for (let i = 0; i < this.products.length; i++) {
                for (let j = 0; j < this.products[i].productsList.length; j++) {
                    let resource = this.products[i].productsList[j].title.toLowerCase()
                    let seacrh = value.toLowerCase()
                    console.log(resource, seacrh)
                    if (resource.indexOf(seacrh) > -1) {
                        console.log("Index", resource.indexOf(seacrh))
                        console.log(this.products[i].productsList[j]);
                        this.searchedResources.push(this.products[i].productsList[j])
                    }
                }
            }
            console.log("Emitted", this.searchedResources)
            this.selectedItem.emit(this.searchedResources);

        }
    }


    // Select the product an emit the product to display resources and set the current product to the service
    selectProduct(product: any) {
        this.selectedProduct = product;
        this.appService.setCurrentProduct(this.selectedProduct);
        this.selectedItem.emit(this.selectedProduct.productsList);
    }
    ngOnInit() {
        this.getAllProducts();
    }
}