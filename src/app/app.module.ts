// importing libraries
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from "@angular/platform-browser";

// importing components
import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/Nav-Menu/navmenu.component';
import { ProductsHomeComponent } from './components/Products-Home/products-home.component';
import { SearchBarComponent } from './components/Search-Bar/search-bar.component'
import { EstimateProductsComponent } from "./components/Estimate-Products/estimate-products.component";
import { ResourcesListComponent } from "./components/Products-List/resources-list.component";
import { NotificationComponent } from './components/Notification/notification.component'
import { VirtualMachineComponent } from "./components/Products/Virtual-machine/virtual-machine.component";
import { AdditionalCPUComponent } from "./components/Products/Additional-CPU/addtional-cpu.component";
import { AdditionalRAMComponent } from "./components/Products/Additional-RAM/addtional-ram.component";
import { StorageComponent } from "./components/Products/Storage/storage.component";
import { RoutableExternalIPComponent } from "./components/Products/Routable-External-IP/routable-external-ip.component";
import { VPNServiceComponent } from "./components/Products/VPN-Service/vpn-service.component";
import { NetworkLogsComponent } from "./components/Products/Network-logs/network-logs.component";
import { ServerLogsComponent } from "./components/Products/Server-logs/server-logs.component";
import { VulnerabilityComponent } from "./components/Products/Vulnerability/vulnerability.component";
import { ObjectWebStorageComponent } from "./components/Products/Object-web-storage/object-web-storage.component";
import { StandardDailyBackupComponent } from "./components/Products/Standard-daily-backup/standard-daily-backup.component";
import { OnDemandBackupComponent } from "./components/Products/On-demand-backup/on-demand-backup.component";
import { PatchManagemantComponent } from "./components/Products/Patch-managemant/patch-managemaent.component";
import { WindowsComponent } from "./components/Products/Windows/windows.component";
import { RedHatEnterpriseLinuxComponent } from "./components/Products/Red-hat-enterprise-linux/red-hat-enterprise-linux.component";
import { CentosComponent } from "./components/Products/Centos/centos.component";


// importing 3rd party libraries
// import { SchemaFormModule, WidgetRegistry, DefaultWidgetRegistry } from "angular2-schema-form";
import { MyDatePickerModule } from 'mydatepicker';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

// importing services
import { AppService } from "./components/Services/app.service";
import { ApiService } from "./components/Services/api.service";
import { NotificationService } from './components/Services/notification.service'
import { AddComponentServive } from "./components/Services/add-component.service";
import { LoaderService } from "./components/Services/loader.service";


// importing directive

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        ProductsHomeComponent,
        SearchBarComponent,
        EstimateProductsComponent,
        ResourcesListComponent,
        NotificationComponent,
        VirtualMachineComponent,
        AdditionalCPUComponent,
        AdditionalRAMComponent,
        StorageComponent,
        RoutableExternalIPComponent,
        VPNServiceComponent,
        NetworkLogsComponent,
        ServerLogsComponent,
        VulnerabilityComponent,
        ObjectWebStorageComponent,
        StandardDailyBackupComponent,
        OnDemandBackupComponent,
        PatchManagemantComponent,
        WindowsComponent,
        RedHatEnterpriseLinuxComponent,
        CentosComponent,

    ],
    entryComponents: [VirtualMachineComponent, AdditionalCPUComponent, AdditionalRAMComponent, StorageComponent, RoutableExternalIPComponent,
        VPNServiceComponent , NetworkLogsComponent, ServerLogsComponent,VulnerabilityComponent, ObjectWebStorageComponent, StandardDailyBackupComponent,OnDemandBackupComponent,PatchManagemantComponent,
        WindowsComponent,RedHatEnterpriseLinuxComponent,CentosComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        MyDatePickerModule,
        SlimLoadingBarModule.forRoot(),
        RouterModule.forRoot([
            { path: '', redirectTo: 'products-home', pathMatch: 'full' },
            { path: 'home', component: ProductsHomeComponent },
            { path: 'products-home', component: ProductsHomeComponent },
        ]),
        // SchemaFormModule,
    ],
    exports: [BrowserModule, SlimLoadingBarModule],
    bootstrap: [AppComponent],
    providers: [AppService, ApiService, NotificationService, AddComponentServive,LoaderService]
})
export class AppModule {
}
