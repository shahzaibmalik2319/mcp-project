export class navBar {
    navBarList = [
        {
            "name": "Featured",
            "routeName": "featured",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Virtual Machines",
                    "shortkey": "featuredvm",
                    "text": "Privision Windows and Linux virtual machines in seconds",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            },
                            "CheckMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Check me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Storage",
                    "shortkey": "featuredstorage",
                    "text": "Durable, highly-available, and massively-scalable cloud storage",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "description": {
                                "type": "string",
                                "description": "text"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "SQL Database",
                    "shortkey": "featuredsqldb",
                    "text": "Managed relational SQL Database as a service",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "App Service",
                    "shortkey": "featuredappservice",
                    "text": "Quickly create powerful cloud apps for web and mobile",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Cosmos DB",
                    "shortkey": "featuredazuredb",
                    "text": "Globally distributed, multi-model database for any scale",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Compute",
            "routeName": "compute",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Virtual Machines",
                    "shortkey": "computevm",
                    "text": "Privision Windows and Linux virtual machines in seconds",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Virtual Machine Scale Sets",
                    "shortkey": "vmscale",
                    "text": "Manage and scale up to thousands of Linux and Windows virtual machines",
                    "pic": "./../../../assets/img/storage-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "App Service",
                    "shortkey": "computeappservice",
                    "text": "Quickly create powerful cloud apps for web and mobile",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Functions",
                    "shortkey": "functions",
                    "text": "Process events with serverless code",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Batch",
                    "shortkey": "batch",
                    "text": "Cloud-scale job scheduling and compute management",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Service Fabric",
                    "shortkey": "servicefab",
                    "text": "Develop microservices and orchestrate containers on Windows or Linux",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Cloud Services",
                    "shortkey": "cloudservice",
                    "text": "Create highly-available, infinitely-scalable cloud applications and APIs",
                    "pic": "./../../../assets/img/virtual-icon.png",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Networking",
            "routeName": "networking",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Application Gateway",
                    "shortkey": "appgateway",
                    "text": "Build scalable and highly-available web front ends in Azure",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "VPN Gateway",
                    "shortkey": "vpngateway",
                    "text": "Establish secure, cross-premises connectivity",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure DNS",
                    "shortkey": "azuredns",
                    "text": "Build scalable and highly-available web front ends in Azure",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Content Delivery Network",
                    "shortkey": "condelnetwork",
                    "text": "Host your DNS domain in Azure",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Traffic Manager",
                    "shortkey": "traffmanager",
                    "text": "Route incoming traffic for high performance and availability",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "ExpressRoute",
                    "shortkey": "exproute",
                    "text": "Dedicated private network fiber connections to Azure",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Bandwidth",
                    "shortkey": "bandwidth",
                    "text": "Data transferred out of Azure data centers",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Network Watcher",
                    "shortkey": "netwatcher",
                    "text": "Network performance monitoring and diagnostics solution",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "IP Addresses",
                    "shortkey": "networkingipadd",
                    "text": "A dynamic or reserved address used to identify a given Virtual Machine or Cloud Service.",
                    "pics": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Storage",
            "routeName": "storage",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Storage",
                    "shortkey": "storage2",
                    "text": "Durable, highly-available, and massively-scalable cloud storage",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Data Lake Store",
                    "shortkey": "storagedatalakestore",
                    "text": "Hyperscale repository for big data analytics workloads",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "StorSimple",
                    "shortkey": "storagestorsimple",
                    "text": "Lower costs with an enterprise hybrid cloud storage solution",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Backup",
                    "shortkey": "backup",
                    "text": "Simple and reliable server backup to the cloud",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Site Recovery",
                    "shortkey": "siterecovery",
                    "text": "Orchestrate protection and recovery of private clouds",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Web + Mobile",
            "routeName": "webMobile",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "App Service",
                    "shortkey": "webMobileappservice",
                    "text": "Quickly create powerful cloud apps for web and mobile",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Logic Apps",
                    "shortkey": "logicapps",
                    "text": "Automate the access and use of data across clouds without writing code",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Content Delivery Network",
                    "shortkey": "condelnetwork",
                    "text": "Ensure secure, reliable content delivery with broad global reach",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Media Services",
                    "shortkey": "mediaservice",
                    "text": "Encode, store, and stream video and audio at scale",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Search",
                    "shortkey": "azuresearch",
                    "text": "Fully-managed search-as-a-service",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Containers",
            "routeName": "containers",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Container Service",
                    "shortkey": "conservice",
                    "text": "Scale and orchestrate containers using Kubernetes, DC/OS or Docker Swarm",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Container Instances",
                    "shortkey": "coninstance",
                    "text": "Easily run containers with a single command",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Container Registry",
                    "shortkey": "conregistry",
                    "text": "Store and manage container images across all types of Azure deployments",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Service Fabric",
                    "shortkey": "serfabric",
                    "text": "Develop microservices and orchestrate containers on Windows or Linux",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Batch",
                    "shortkey": "containersbatch",
                    "text": "Cloud-scale job scheduling and compute management",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Databases",
            "routeName": "database",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "SQL Database",
                    "shortkey": "databasesqldb",
                    "text": "Managed relational SQL Database as a service",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Database for MySQL",
                    "shortkey": "databaseazuredbsql",
                    "text": "Managed MySQL database service for app developers",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Database for PostgreSQL",
                    "shortkey": "azuredbpostsql",
                    "text": "Managed PostgreSQL database service for app developers",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "SQL Data Warehouse",
                    "shortkey": "sqldbwarehouse",
                    "text": "Elastic data warehouse as a service with enterprise-class features",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "SQL Server Stretch Database",
                    "shortkey": "sqlserverdb",
                    "text": "Dynamically stretch on-premises SQL Server databases to Azure",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Cosmos DB",
                    "shortkey": "sqldb",
                    "text": "Globally distributed, multi-model database for any scale",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Redis Cache",
                    "shortkey": "rediscache",
                    "text": "Power applications with high-throughput, low-latency data access",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Data Factory",
                    "shortkey": "datafac",
                    "text": "Orchestrate and manage data transformation and movement",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Data + Analytics",
            "routeName": "dataAnalytics",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "HDInsight",
                    "shortkey": "hdinsigth",
                    "text": "Managed relational SQL Database as a service",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Machine Learning",
                    "shortkey": "machinelearn",
                    "text": "Easily build, deploy, and manage predictive analytics solutions",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Stream Analytics",
                    "shortkey": "streamanaly",
                    "text": "Real-time data stream processing from millions of IoT devices",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Data Lake Analytics",
                    "shortkey": "datalakeanaly",
                    "text": "Distributed analytics service that makes big data easy",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Data Catalog",
                    "shortkey": "datacatalog",
                    "text": "Get more value from your enterprise data assets",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Analysis Services",
                    "shortkey": "azureanalyservices",
                    "text": "Enterprise grade analytics engine as a service",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Dynamics 365 for Customer Insights",
                    "shortkey": "dynacustomer",
                    "text": "Transform your customer data into actionable insights",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "AI + Cognitive Service",
            "routeName": "aiCognitive",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Cognitive Services",
                    "shortkey": "cogservices",
                    "text": "Add smart API capabilities to enable contextual interactions",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Internet of Things",
            "routeName": "iot",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "IoT Hub",
                    "shortkey": "iothub",
                    "text": "Connect, monitor, and control billions of IoT assets",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Event Hubs",
                    "shortkey": "eventhub",
                    "text": "Receive telemetry from millions of devices",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Stream Analytics",
                    "shortkey": "streamanaly",
                    "text": "Real-time data stream processing from millions of IoT devices",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Machine Learning",
                    "shortkey": "machinelearn",
                    "text": "Easily build, deploy, and manage predictive analytics solutions",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Notification Hubs",
                    "shortkey": "nothub",
                    "text": "Send push notifications to any platform from any back end",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Time Series Insights",
                    "shortkey": "timeserinsigth",
                    "text": "Instantly explore and analyze time-series data",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Event Grid",
                    "shortkey": "eventgrid",
                    "text": "Get reliable event delivery at massive scale",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Enterprise Intregation",
            "routeName": "enterprise",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Logic Apps",
                    "shortkey": "logicapp",
                    "text": "Automate the access and use of data across clouds without writing code",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Service Bus",
                    "shortkey": "servicebus",
                    "text": "Connect across private and public cloud environments",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "API Management",
                    "shortkey": "apimanagment",
                    "text": "Publish APIs to developers, partners, and employees securely and at scale",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "StorSimple",
                    "shortkey": "storsimple",
                    "text": "Lower costs with an enterprise hybrid cloud storage solution",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "SQL Server Stretch Database",
                    "shortkey": "sqlserverdb",
                    "text": "Dynamically stretch on-premises SQL Server databases to Azure",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Data Catalog",
                    "shortkey": "datacatalog",
                    "text": "Get more value from your enterprise data assets",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Data Factory",
                    "shortkey": "datafac",
                    "text": "Orchestrate and manage data transformation and movement",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Event Grid",
                    "shortkey": "eventgrid",
                    "text": "Get reliable event delivery at massive scale",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Security + Identity",
            "routeName": "securityIntregation",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Security Center",
                    "shortkey": "seccenter",
                    "text": "Prevent, detect, and respond to threats with increased visibility",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Key Vault",
                    "shortkey": "keyvault",
                    "text": "Safeguard and maintain control of keys and other secrets",
                    "pic": "",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Active Directory",
                    "shortkey": "azureactivedirec",
                    "text": "Synchronize on-premises directories and enable single sign-on",
                    "pic": "",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Active Directory B2C",
                    "shortkey": "azureactiveb2c",
                    "text": "Consumer identity and access management in the cloud",
                    "pic": "",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Developers Tools",
            "routeName": "developers-tools",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Visual Studio Team Services",
                    "shortkey": "visualteamservice",
                    "text": "Services for teams to share code, track work, and ship software",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Application Insights",
                    "shortkey": "appinsigth",
                    "text": "Detect, triage, and diagnose issues in your web apps and services",
                    "pic": "",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "API Management",
                    "shortkey": "apimanagement",
                    "text": "Publish APIs to developers, partners, and employees securely and at scale",
                    "pic": "",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "HockeyApp",
                    "shortkey": "hockeyapp",
                    "text": "Deploy mobile apps, collect feedback and crash reports, and monitor usage",
                    "pic": "",
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        },
        {
            "name": "Monitoring + Managment",
            "routeName": "monitoring-managment",
            "isCollapse": true,
            "productsList": [
                {
                    "title": "Application Insights",
                    "shortkey": "appinsigth",
                    "text": "Detect, triage, and diagnose issues in your web apps and services",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Log Analytics",
                    "shortkey": "loganaly",
                    "text": "Collect, search, and visualize machine data from on-premises and cloud",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Automation",
                    "shortkey": "automation",
                    "text": "Simplify cloud management with process automation",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Backup",
                    "shortkey": "backup",
                    "text": "Simple and reliable server backup to the cloud",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Site Recovery",
                    "shortkey": "siterecovery",
                    "text": "Orchestrate protection and recovery of private clouds",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Scheduler",
                    "shortkey": "scheduler",
                    "text": "Run your jobs on simple or complex recurring schedules",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Azure Monitor",
                    "shortkey": "azuremonitor",
                    "text": "Highly granular and real-time monitoring data for any Azure resource",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                },
                {
                    "title": "Insight & Analytics",
                    "shortkey": "insigthanaly",
                    "text": "Easily search, correlate, and analyze data from the cloud",
                    "pic": "",
                    "pricePerHour": 0.14,"quantity":1,"totalPrice":104.16,
                    "mySchema": {
                        "properties": {
                            "email": {
                                "type": "string",
                                "description": "Email",
                                "format": "email",
                                "placeholder": "Email",
                                "class": "col-sm-6"
                            },
                            "password": {
                                "type": "string",
                                "description": "Password"
                            },
                            "rememberMe": {
                                "type": "boolean",
                                "default": false,
                                "description": "Remember me"
                            }
                        },
                        "required": [
                            "email",
                            "password",
                            "rememberMe"
                        ]
                    }
                }
            ]
        }

    ]
}