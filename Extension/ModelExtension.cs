using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace McpProject.Business
{
    public static class ModelExtension
    {
        public static T ToModel<T>(this Dictionary<string, object> row) where T : IModelMap<T>, new()
        {
            return new T().ToModel(row);
        }
        public static string SafeString(this object item)
        {
            return DBNull.Value == item ? string.Empty : ((string)item).Trim();
        }

        public static int? SafeInt(this object item)
        {
            return (int?) (DBNull.Value == item ? null : item);
        }
        public static int SafeIntReturnInt(this object item)
        {
            return (DBNull.Value == item ? 0 : int.Parse(item.ToString()));
        }
        public static string SafeDate(this object item)
        {
            return DBNull.Value == item ? string.Empty : ((DateTime)item).ToString(CultureInfo.InvariantCulture);
        }
        public static string ByteToString(this object item)
        {
            return Convert.ToString(item);
        }

        public static bool? SafeBool(this object item)
        {
            return DBNull.Value == item ? null : (bool?) item;
        }
        public static int ByteToInt(this object item)
        {
            return Convert.ToInt32(item);
        }
    }
}
