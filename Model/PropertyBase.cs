using System.Collections.Generic;
using System.Data;
using McpProject.Business;

namespace McpProject.Model
{
    public class EmailProperty : PropertyBase
    {

    }

    public class PasswordProperty : PropertyBase
    {

    }

    public class RememberMeProperty : PropertyBase
    {

    }

    public class PropertyBase : IModelMap<PropertyBase>
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public string Type {get;set;}
        public string Default {get;set;}
        public string Description {get;set;}
        public string Format {get;set;}
        public string Placeholder {get;set;}
        public string Class {get;set;}

        PropertyBase IModelMap<PropertyBase>.ToModel(Dictionary<string, object> row)
        {
            throw new System.NotImplementedException();
        }
    }
}
