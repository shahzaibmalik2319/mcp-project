using System.Collections.Generic;
using System.Data;
using McpProject.Business;

namespace McpProject.Model
{
    public class Properties : IModelMap<Properties>
    {
        public EmailProperty Email {get;set;}
        public PasswordProperty Password {get;set;}
        public RememberMeProperty RememberMe {get;set;}

        Properties IModelMap<Properties>.ToModel(Dictionary<string, object> row)
        {
            throw new System.NotImplementedException();
        }
    }
}
