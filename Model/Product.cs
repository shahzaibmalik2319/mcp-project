using System;
using System.Collections.Generic;
using McpProject.Business;

namespace McpProject.Model
{
    public class Product : IModelMap<Product>
    {
        public int Id {get;set;}
        public int CategoryId {get;set;}
        public string Title {get;set;}
        public string ShortKey {get;set;}
        public string Text {get;set;}
        public string Pic {get;set;}
        public double PricePerHour {get;set;}
        public int Quantity {get;set;}
        public double PricePerMonth {get;set;}
        public double PricePerDay {get;set;}
        public double TotalCost {get;set;}
        public double Surcharge {get;set;}
        public int StorageType {get;set;}
        public int VMType {get;set;}
        public int OSType {get;set;}
        public string Component {get;set;}

        Product IModelMap<Product>.ToModel(Dictionary<string, object> row)
        {
            var model = new Product();
            int id = (int)row["id"];
            model.Id = id;
            model.CategoryId = Convert.ToInt32(row["category"]);
            model.Title = Convert.ToString(row["title"]);
            model.ShortKey = Convert.ToString(row["short_key"]);
            model.Text = Convert.ToString(row["text"]);
            model.Pic = Convert.ToString(row["pic"]);
            model.PricePerHour = Convert.ToDouble(row["price_hour"]);
            model.PricePerDay = Convert.ToDouble(row["price_day"]);
            model.PricePerMonth =  Convert.ToDouble(row["price_month"]);
            model.Quantity = Convert.ToInt32(row["quantity"]);
            model.TotalCost = Convert.ToDouble(row["total_cost"]);
            model.Surcharge = Convert.ToDouble(row["surcharge"]);
            model.StorageType = !Convert.IsDBNull(row["storage_type_id"]) ? Convert.ToInt32(row["storage_type_id"]) : -1;
            model.VMType = !Convert.IsDBNull(row["vm_type_id"]) ? Convert.ToInt32(row["vm_type_id"]) : -1;
            model.OSType = !Convert.IsDBNull(row["os_type_id"]) ? Convert.ToInt32(row["os_type_id"]) : -1;
            model.Component = Convert.ToString(row["component"]);

            ProductProperties schema = new ProductProperties();

            return model;
        }
    }
}
