using System.Collections.Generic;
using System.Data;
using McpProject.Business;

namespace McpProject.Model
{
    public class Field  : IModelMap<Field>
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public string Type {get;set;}
        public string Default {get;set;}
        public string Description {get;set;}
        public string Format {get;set;}
        public string Placeholder {get;set;}
        public string Class {get;set;}

        Field IModelMap<Field>.ToModel(Dictionary<string, object> row)
        {
            throw new System.NotImplementedException();
        }
    }
}
