using System;
using System.Collections.Generic;
using System.Data;
using McpProject.Business;

namespace McpProject.Model
{
    public class ProductCategory : IModelMap<ProductCategory>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RouteName {get;set;}
        public bool IsCollapse {get;set;}
        public List<Product> ProductsList {get;set;}

        public ProductCategory ToModel(Dictionary<string, object> row)
        {
            var model = new ProductCategory();
            model.Id = Convert.ToInt32(row["id"]);
            model.Name = Convert.ToString(row["name"]);
            model.RouteName = Convert.ToString(row["route_name"]);
            model.IsCollapse = Convert.ToBoolean(row["collapse"]);

            return model;
        }
    }
}
