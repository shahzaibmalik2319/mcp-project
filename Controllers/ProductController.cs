using System.Collections.Generic;
using McpProject.Business;
using Microsoft.AspNetCore.Mvc;
using McpProject.Model;

namespace McpProject.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        public ProductService _productService { get;set; }

        public ProductController()
        {
            _productService = new ProductService();
        }


        [HttpGet]
        public IEnumerable<ProductCategory> Products()
        {
            var model = _productService.GetProducts();
            return model;
        }
    }

}
