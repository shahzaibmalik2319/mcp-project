using System.Collections.Generic;
using McpProject.Business;
using McpProject.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace McpProject.Controllers
{
    [Route("api/[controller]")]
    public class LookupController : Controller
    {
        public LookupService _lookupService { get;set; }

        public LookupController()
        {
            _lookupService = new LookupService();
        }


        [HttpGet("{type}")]
        public ActionResult Lookup(string type)
        {
            // CalculatorContext context = HttpContext.RequestServices.GetService(typeof(Mcp.Data.CalculatorContext)) as CalculatorContext;
            // var result = context.GetLookup(type);
            // System.Console.Write("RETRIEVED FROM DB: ");
            // System.Console.WriteLine(result);
            // //var result = _lookupService.GetLookup(type);
            // return Ok(Json(result));

            var model = _lookupService.GetLookup(type);
            return Ok(Json(model));
        }
    }

}
