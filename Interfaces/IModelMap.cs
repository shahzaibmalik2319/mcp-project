using System.Collections.Generic;
using System.Data;

namespace McpProject.Business
{
    public interface IModelMap<T>
    {
        T ToModel(Dictionary<string, object> row);
    }
}
