using System;
using System.Collections.Generic;
using System.Data;
using McpProject.Model;
using MySql.Data.MySqlClient;

namespace McpProject.Data
{
    public class CalculatorContext
    {
        public string ConnectionString { get; set; }

        public CalculatorContext()
        {
        }

        public CalculatorContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public class FunctionResult
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }

            public FunctionResult()
            {
                Success = true;
                ErrorMessage = "";
            }

            public FunctionResult(string errorMessage)
            {
                Success = false;
                ErrorMessage = errorMessage;
            }

            public FunctionResult(bool success, string message)
            {
                Success = success;
                ErrorMessage = message;
            }
        }

        public class DBAccessTableResult : FunctionResult
        {
            public DataTable DataTable { get; private set; }

            public DBAccessTableResult()
                : base()
            {
                DataTable = new DataTable();
            }

            public DBAccessTableResult(string errorMessage)
                : base(errorMessage)
            {
                DataTable = new DataTable();
            }

            public DBAccessTableResult(DataTable dataTable)
                : base()
            {
                DataTable = dataTable;
            }
        }

        public DBAccessTableResult ExecuteWithTable(string storedProcedureName, List<MySqlParameter> parameters, CommandType CommandType=CommandType.StoredProcedure)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(ConnectionString))
                {
                    using (var command = new MySqlCommand(storedProcedureName, connection))
                    {
                        command.CommandType = CommandType;//CommandType.StoredProcedure;

                        if (parameters != null && parameters.Count > 0)
                        {
                            command.Parameters.AddRange(parameters.ToArray());
                        }

                        connection.Open();

                        DataTable dt = new DataTable();
                        var dr = command.ExecuteReader(CommandBehavior.CloseConnection);

                        DataTable dtSchema = dr.GetSchemaTable();
                        List<DataColumn> listCols = new List<DataColumn>();
                        if (dtSchema != null)
                        {
                            foreach (DataRow drow in dtSchema.Rows)
                            {
                                string columnName = System.Convert.ToString(drow["ColumnName"]);
                                DataColumn column = new DataColumn(columnName, (Type)(drow["DataType"]));
                                column.Unique = (bool)drow["IsUnique"];
                                column.AllowDBNull = (bool)drow["AllowDBNull"];
                                column.AutoIncrement = (bool)drow["IsAutoIncrement"];
                                listCols.Add(column);
                                dt.Columns.Add(column);
                            }

                        }

                        while (dr.Read())
                        {
                            DataRow dataRow = dt.NewRow();
                            for (int i = 0; i < listCols.Count; i++)
                            {
                                dataRow[((DataColumn)listCols[i])] = dr[i];
                            }

                            dt.Rows.Add(dataRow);
                        }

                        if (dt.Rows.Count > 0)
                        {
                            return new DBAccessTableResult(dt);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new DBAccessTableResult("Error executing stored procedure " + storedProcedureName + ": " + ex.Message);
            }

            return new DBAccessTableResult("No data.");
        }

        public string GetLookup(string lookupType)
        {
            var lookupJson = "[";

            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                string procedureName = "spGetLookupJson";
                MySqlCommand cmd = new MySqlCommand(procedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pTableName", lookupType.ToLower());
                cmd.Parameters.AddWithValue("@pId", null);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lookupJson += (string)reader.GetString(0) + ",";
                    }
                }

                lookupJson = lookupJson.TrimEnd(','); // Remove the trailing comma
            }

            return lookupJson + "]";
        }

        public List<Dictionary<string, object>> getDataByProcedure(String procedureName, List<MySqlParameter> parameters)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            DataTable dt = new DataTable();

            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(procedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                if (parameters != null) {
                    cmd.Parameters.Add(parameters);
                }

                using (var reader = cmd.ExecuteReader()) {
                    while (reader.Read())
                    {
                        Dictionary<string, object> row = new Dictionary<string, object>();
                        for (int i=0; i < reader.FieldCount; i++)
                        {
                            row.Add(reader.GetName(i), reader[i]);
                            //row[i] = reader[i];
                        }

                        rows.Add(row);
                    }
                }
            }

            return rows;
        }
    }
}
